let navItems = document.querySelector("#navSession");
let registerBtn = document.querySelector("#registerBtn");
let profileLink = document.querySelector("#profileLink");

let userToken = localStorage.getItem("token");
console.log(userToken);

let userId = localStorage.getItem("id");
console.log(userId);

if (!userToken) {

	navItems.innerHTML = 
		`
			<li class="nav-item mb-0">
				<a href="./pages/login.html" class="btn btn-outline-orange"> Log In </a>
			</li>
		`

	registerBtn.innerHTML =
		`
			<li class="nav-item mb-0">
				<a href="./pages/register.html" class="btn btn-orange"> Register </a>
			</li>
		`

} else {

	profileLink.innerHTML =
		`
			<li class="nav-item mb-0">
				<a href="./pages/profile.html?id=${userId}" class="nav-link">Profile</a>
			</li>
		`

	navItems.innerHTML = 
		`
			<li class="nav-item mb-0">
				<a href="./pages/logout.html" class="nav-link"> Logout </a>
			</li>
		`

}