let params = new URLSearchParams(window.location.search);
let userID = params.get('userID');
let token = localStorage.getItem("token");

let editProfileButton = document.querySelector('#editProfileButton');
let changePassButton = document.querySelector("#changePassButton");

let editPassForm = document.querySelector("#editPassword");
let current = document.querySelector("#currentPass");
let pass1 = document.querySelector("#password1");
let pass2 = document.querySelector("#password2");
let password;

if (token === null || !token) {

	// Unauthenticated user
	alert('You must login first');
	// Redirect user to login page
	window.location.href="./login.html";

} else {

	fetch(`https://enigmatic-mountain-78703.herokuapp.com/api/users/details`, {
		headers: {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {

		console.log(data)
		
		editProfileButton.innerHTML = 
			`
				<a class="nav-link" href="./editProfile.html?userID=${data._id}">Edit Profile</a>
				
			`;
		changePassButton.innerHTML = 
			`
				<a class="nav-link active" href="./changePassword.html?userID=${data._id}">Change Password</a>
				
			`;

		editPassForm.addEventListener("submit", (e) => {

			e.preventDefault();

			
			let current = document.querySelector("#currentPass").value;
	
			// Fetch Request for checking if current password match
			fetch('https://enigmatic-mountain-78703.herokuapp.com/api/users/password-check', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					userID: userID,
					password: current
				})
			})
			.then(res => res.json())
			.then(data => {

				console.log(data)

				if (data === false) {

					alert("Please enter your current password")

				} else {

					if ((pass1.value === pass2.value) && (pass1.value !== '' && pass2.value !== '')) {

							password = pass1.value

					} else if ((pass1.value === '' && pass2.value === '')) {

						alert("Password cannot be empty")

					} else {

						alert(`Password doesn't match`)

					}

					// Fetch request for updating password
					fetch(`https://enigmatic-mountain-78703.herokuapp.com/api/users/${userID}`, {
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							'Authorization' : `Bearer ${token}`
						},
						body: JSON.stringify({
							userID : userID,
							password: password
						})
					})
					.then(res => res.json())
					.then(data => {

						console.log(data)

						if (data === true) {
								
								alert(`You have updated your profile!`)
								window.location.replace("./profile.html")

							} else {

								alert(`Something went wrong...`)
							}
					})
					
				}

			})

		})

	})

}
