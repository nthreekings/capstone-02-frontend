let navItems = document.querySelector("#navSession");
let registerBtn = document.querySelector("#registerBtn");
let profileLink = document.querySelector("#profileLink");
let guestNav = document.querySelector("#guestNav");
let coursesLink = document.querySelector("#coursesLink");

let userToken = localStorage.getItem("token");
console.log(userToken);

let userId = localStorage.getItem("id");
console.log(userId);

if (!userToken) {
	guestNav.innerHTML = 
		`
			<li class="nav-item mb-0">
				<a href="../index.html" class="nav-link"> Home </a>
			</li>

			<li class="nav-item mb-0">
				<a href="#main-section" class="nav-link">How it works</a>
			</li>

			<li class="nav-item mb-0">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>

			<li class="nav-item mb-0">
				<a href="#" class="nav-link">FAQs</a>
			</li>

		`

	navItems.innerHTML = 
		`
			<li class="nav-item mb-0">
				<a href="./login.html" class="btn btn-outline-orange"> Log In </a>
			</li>
		`

	registerBtn.innerHTML =
		`
			<li class="nav-item mb-0">
				<a href="./register.html" class="btn btn-orange"> Register </a>
			</li>
		`

} else {

	coursesLink.innerHTML = 
		`
			<li class="nav-item ">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>

		`

	navItems.innerHTML = 
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
		`

}