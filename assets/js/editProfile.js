let params = new URLSearchParams(window.location.search);
let userID = params.get('userID');
let token = localStorage.getItem("token");

let editProfileButton = document.querySelector('#editProfileButton');
let changePassButton = document.querySelector("#changePassButton");

let formEdit = document.querySelector("#editProfile");
let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let mobileNumber = document.querySelector("#mobileNumber");
let email = document.querySelector("#userEmail");

if (token === null || !token) {

	// Unauthenticated user
	alert('You must login first');
	// Redirect user to login page
	window.location.href="./login.html";

} else {
	fetch(`https://enigmatic-mountain-78703.herokuapp.com/api/users/details`, {
		headers : {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {

		console.log(data)

		editProfileButton.innerHTML = 
			`
				<a class="nav-link active" href="./editProfile.html?userID=${data._id}">Edit Profile</a>
				
			`;
		changePassButton.innerHTML = 
			`
				<a class="nav-link" href="./changePassword.html?userID=${data._id}">Change Password</a>
				
			`;

		firstName.placeholder = data.firstName;
		lastName.placeholder = data.lastName;
		mobileNumber.placeholder = data.mobileNo;
		email.placeholder = data.email;

		firstName.value = data.firstName;
		lastName.value = data.lastName;
		mobileNumber.value = data.mobileNo;
		email.value = data.email;

		formEdit.addEventListener("submit", (e) => {

			e.preventDefault();

			let userID = params.get('userID');
			let firstName = document.querySelector("#firstName").value;
			let lastName = document.querySelector("#lastName").value;
			let mobileNo = document.querySelector("#mobileNumber").value;
			let email = document.querySelector("#userEmail").value;

			fetch('https://enigmatic-mountain-78703.herokuapp.com/api/users/', {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					userID: userID,
					firstName: firstName,
					lastName: lastName,
					mobileNo: mobileNo,
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if (data === true) {
					
					alert(`You have updated your profile!`)
					window.location.replace("./profile.html")

				} else {

					alert(`Something went wrong...`)
				}
			})
		})
	})
}
