let params = new URLSearchParams(window.location.search);

let courseId = params.get('courseId');

let token = localStorage.getItem('token');

fetch(`https://enigmatic-mountain-78703.herokuapp.com/api/courses/${courseId}`, {
	method: 'PUT',
	headers: {
		'Authorization' : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	console.log(data);

	if(data === true){

		// Delete course successful
	    // Redirect to courses page
	    window.location.replace("./courses.html");

	} else {

	    // Error in deleting a course
	    alert("Something went wrong");

	}
})