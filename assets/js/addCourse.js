let adminUser = localStorage.getItem("isAdmin");
let formSubmit = document.querySelector("#createCourse");

let token = localStorage.getItem("token");

if (adminUser == "false" || !adminUser) {

    alert("You don't have administrator rights to this page!")
    window.location.replace("./courses.html")

} else {

	formSubmit.addEventListener("submit", (e) => {

		e.preventDefault();

		let courseName = document.querySelector("#courseName").value;
		let courseDescription = document.querySelector("#courseDescription").value;
		let coursePrice = document.querySelector("#coursePrice").value;

		if ((courseName !== '' && courseDescription !== '' && coursePrice !== '') && (coursePrice > 0)) {

			fetch('https://enigmatic-mountain-78703.herokuapp.com/api/courses', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					name: courseName,
					description: courseDescription,
					price: coursePrice
				})
			})
			.then(res => res.json())
			.then(data => {

				console.log(data);

				if(data === true){

					alert("Course successfully added!");
					window.location.replace("./courses.html");

				} else {

					alert("Something went wrong.");

				}

			})

		} else if (coursePrice < 0) {

			alert("Price cannot be negative")

		} else {

			alert("Fields cannot be empty")

		}



	})

}

