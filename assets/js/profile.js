// "window.location.search" return the query string part of the url
console.log(window.location.search);

// instantiate a URLSearchParams object so we can execute methods to access a specific parts of the query string
let params = new URLSearchParams(window.location.search);

// the "has" method checks if the "courseId" key exists in the URL query string 
// the method returns true if the key exists
console.log(params.has('id'));

// returns the value of the key passed in as an argument
console.log(params.get('id'));

let userID = params.get('id');

let token = localStorage.getItem('token');

// select ids where the data will be put
let name = document.querySelector("#name");
let mobileNo = document.querySelector("#mobileNo");
let email = document.querySelector("#email");
let editProfileButton = document.querySelector('#editProfileButton');
let changePassButton = document.querySelector("#changePassButton");

if(!token || token === null) {

	// Unauthenticated user
	alert('You must login first');
	// Redirect user to login page
	window.location.href="./login.html";

} else {

	// fetch request that will get user details
	fetch('https://enigmatic-mountain-78703.herokuapp.com/api/users/details', {
		headers: {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(userData => {

		console.log(userData.isAdmin);

		name.innerHTML = `${userData.firstName} ${userData.lastName}`;
		mobileNo.innerHTML = userData.mobileNo;
		email.innerHTML = userData.email;
		editProfileButton.innerHTML = 
			`
				<a class="nav-link" href="./editProfile.html?userID=${userData._id}">Edit Profile</a>
				
			`;
		changePassButton.innerHTML = 
			`
				<a class="nav-link" href="./changePassword.html?userID=${userData._id}">Change Password</a>
				
			`;

		if (userData.isAdmin === true) {


			document.querySelector("#enrollContainer").innerHTML = ``

		} else {

			if (userData.enrollments.length < 1 ){

				document.querySelector("#enrollContainer").innerHTML = 
					`
						<p class="mt-5 text-center">No courses enrolled.</p>

					`

			} else {

				userData.enrollments.map(enrollments => {

					let courseId = enrollments.courseId;

					// fetch request that will get course details
					fetch(`https://enigmatic-mountain-78703.herokuapp.com/api/courses/${courseId}`)
					.then(res => res.json())
					.then(courseData => {

						console.log(courseData)

						if (courseData.isActive === true) {

							document.querySelector("#mycourse-header").innerHTML = `<h3>My Courses</h3>`

							document.querySelector("#enrollContainer").innerHTML += 
									`
										<div class="card my-3">
											<div class="row card-body">
												<div class="col-md-7">
													<h4 class="card-title">${courseData.name}</h4>
													<p class="card-title">${enrollments.enrolledOn}</p>
												</div>

												<div class="col-md-2 offset-md-3">
													<span class="badge badge-pill badge-success">${enrollments.status}</span>
												</div>
											</div>
										</div>

									`	

						} else {

							document.querySelector("#enrollContainer").innerHTML += ``

						}
								
					})

				})

			}

		}

	})
	
}
