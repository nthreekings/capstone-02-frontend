let adminUser = localStorage.getItem("isAdmin");
let cardFooter;
let courseStatus;
let modalButton = document.querySelector("#adminButton");
let jumbotron = document.querySelector("#course-header");

if(adminUser == "false" || !adminUser) {

	modalButton.innerHTML = 
	`
		<div class="col-md-2">
			<h3>Courses</h3>
		</div>

	`

	jumbotron.innerHTML = ``

} else {

	modalButton.innerHTML =
	`
		<div class="col-md-2">
			<h3>Courses</h3>
		</div>
		<div class="col-md-2 offset-md-8">
			<a href="./addCourse.html" class="btn btn-block btn-orange">
				Add Course
			</a>
		</div>
	`
	fetch('https://enigmatic-mountain-78703.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {

		let sumEnrolled = 0;

		data.map(course => {

			return sumEnrolled += course.enrollees.length;

		})

		jumbotron.innerHTML = 
			`<header class="jumbotron jumbotron-courses text-white text-left">
				<div class="row header-section pt-2">
					<div class="col-md-4 offset-md-1">
						<h5 class="text-mute">No. of Courses</h5>
						<h1 class="display-4 text-white" id="totalCourses">${data.length}</h1>
					</div>

					<div class="col-md-4">
						<h5 class="text-mute">No. of Enrollees</h5>
						<h1 class="display-4 text-white" id="totalEnrollees">${sumEnrolled}</h1>
						<!-- <h1>Browse Course</h1>
						<p class="">Learn and improve skills across business, tech, design and more.</p> 
						<form class="form-inline">
						    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
						    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
					  	</form> -->
					</div>
					<!-- end col -->

					<div class="col-md-3">
						
					</div>
					<!-- end col -->

				</div>
				<!-- end row -->

			</header>`
	})

}

fetch('https://enigmatic-mountain-78703.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {

	console.log(data);

	// Creates a variable that will store the data to be rendered
	let courseData;

	if(data.length < 1) {

		courseData = "No courses available"

	} else {

		courseData = data.map(course => {
			
			if(adminUser == "false" || !adminUser) {

				if (course.isActive === true) {

					return (
					`
						<div class="col-md-4 my-3">
							<div class="card course-card h-100">
								<div class="card-body">
									<h5 class="card-title mt-3">
										<strong>${course.name}</strong>
									</h5>

									<p class="card-text text-sub truncate text-left">
										${course.description}
									</p>

									<div class="d-flex justify-content-between mt-4">
										<p class="mb-0 text-small text-sub">
											<span><i class="fas fa-clock"></i></span>&nbsp;
											${new Date(course.createdOn).toDateString()}
										</p>

										<p class="mb-0 text-small text-sub">
											<span><i class="fas fa-user"></i></span>&nbsp;
											${course.enrollees.length} Enrollees
										</p>
									</div>
									
								</div>
								<div class="card-footer d-flex justify-content-between">
									<a href="./course.html?courseId=${course._id}" value="${course._id}" class="stretched-link"></a>
									<h5 class="card-text text-right price-text">
										<strong>PHP ${course.price}</strong>
									</h5>
								</div>
							</div>
						</div>
					`
			)

				}
				

			} else {

				if (course.isActive === true) {

					courseStatus = 
					`
						<span class="badge badge-live float-left">Live</span>
					`

					cardFooter =
					`	
						<a href="./viewCourse.html?courseId=${course._id}" value="${course._id}" class="dropdown-item" type="button"><span><i class="fas fa-eye mr-3"></i></span>View</a>
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="dropdown-item" type="button"><span><i class="fas fa-pen mr-3"></i></span>Edit</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="dropdown-item" type="button"><span><i class="fas fa-caret-square-down mr-3"></i></span>Archive</a>
						
					`

				} else {

					courseStatus = 
					`
						<span class="badge badge-archived float-left">Archived</span>

					`

					cardFooter =
					`
						<a href="./viewCourse.html?courseId=${course._id}" value="${course._id}" class="dropdown-item" type="button"><span><i class="fas fa-eye mr-3"></i></span>View</a>
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="dropdown-item" type="button"><span><i class="fas fa-pen mr-3"></i></span>Edit</a>
						<a href="./unarchiveCourse.html?courseId=${course._id}" value="${course._id}" class="dropdown-item" type="button"><span><i class="fas fa-caret-square-up mr-3"></i></span>Unarchive</a>
					`

				}
				
				return (
					`
						<div class="col-md-4 my-3">
							<div class="card course-card h-100">
								<div class="card-body">
									${courseStatus}
									<div class="d-flex justify-content-end">
										<div class="btn-group">
										  <a type="button" class="btn dropdown p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										    <span><i class="fas fa-ellipsis-h"></i></span>
										  </a>
										  <div class="dropdown-menu dropdown-menu-right">
										    ${cardFooter}
										  </div>
										</div>
									</div>

									<h5 class="card-title mt-3">
										<strong>${course.name}</strong>
									</h5>

									<p class="card-text text-sub truncate text-left">
										${course.description}
									</p>

									<div class="d-flex justify-content-between mt-4">
										<p class="mb-0 text-small text-sub">
											<span><i class="fas fa-clock"></i></span>&nbsp;
											${new Date(course.createdOn).toDateString()}
										</p>

										<p class="mb-0 text-small text-sub">
											<span><i class="fas fa-user"></i></span>&nbsp;
											${course.enrollees.length} Enrollees
										</p>
									</div>
								</div>
								<div class="card-footer">
									<h5 class="card-text text-right price-text">
										<strong>PHP ${course.price}</strong>
									</h5>
								</div>
							</div>
						</div>
					`
				)

			}

			

		// Replaces the comma's in the array with an empty string
		}).join("");

	}

	document.querySelector("#coursesContainer").innerHTML = courseData;

})