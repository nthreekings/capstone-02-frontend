// return the query string part of the url
console.log(window.location.search);

// instantiate a URLSearchParams object so we can execute methods to access a specific parts of the query string
let params = new URLSearchParams(window.location.search);

// checks if the courseId key exists in the URL query string. returns true if they key exists
console.log(params.has('courseId'));

// returns the value of the key
console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem('token');

// select the ids where the data will be put
let courseName = document.querySelector("#courseName");
let description = document.querySelector("#description");
let enrolleesContainer = document.querySelector("#enrolleesContainer");

// fetch request that will get course details
fetch(`https://enigmatic-mountain-78703.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(courseData => {

	console.log(courseData)

	courseName.innerHTML = courseData.name;
	description.innerHTML = courseData.description;

	if (courseData.enrollees.length < 1){

		enrolleesContainer.innerHTML =
			`
				<p>No enrollees</p>

			`
	} else {

		courseData.enrollees.map(enrollees => {

			let userId = enrollees.userId;

			// fetch request that will get user details
			fetch(`https://enigmatic-mountain-78703.herokuapp.com/api/users/${userId}`, {
				headers: {
					'Content-Type' : 'application/json',
					'Authorization' : `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(userData => {

				console.log(userData)	

				enrolleesContainer.innerHTML += 
						`	
							<div class="card mb-3">
								<div class="card-body">
								<h4 class="card-title">
									${userData.firstName} ${userData.lastName}
								</h4>
								<p class="card-title mb-0">${enrollees.enrolledOn}</p>
								</div>
							</div>

						`			

			})

		})
	}
})