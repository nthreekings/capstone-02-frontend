
let adminUser = localStorage.getItem("isAdmin");
let formEdit = document.querySelector("#editCourse")
let token = localStorage.getItem("token");
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');

let courseName = document.querySelector("#courseName");
let coursePrice = document.querySelector("#coursePrice");
let courseDescription = document.querySelector("#courseDescription");


if (adminUser == "false" || !adminUser) {
    alert("You don't have administrator rights to this page!")
    window.location.replace("./courses.html")
}

else {
    fetch(`https://enigmatic-mountain-78703.herokuapp.com/api/courses/${courseId}`)
    .then(res => res.json())
    .then(data => {

        console.log(data)

        courseName.placeholder = data.name;
        coursePrice.placeholder = data.price;
        courseDescription.placeholder = data.description;

        courseName.value = data.name;
        coursePrice.value = data.price;
        courseDescription.value = data.description;


    })

    formEdit.addEventListener("submit", (e) => {

        e.preventDefault();

        let courseId = params.get('courseId');
        let courseName = document.querySelector("#courseName").value;
        let courseDesc = document.querySelector("#courseDescription").value;
        let coursePrice = document.querySelector("#coursePrice").value;

        fetch('https://enigmatic-mountain-78703.herokuapp.com/api/courses', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                courseId: courseId,
                name: courseName,
                description: courseDesc,
                price: coursePrice
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data === true) {
                alert(`You have updated the course ${courseName}!`)
                window.location.replace("./courses.html")
            }
            else {
                alert(`Something went wrong...`)
            }
        })
    })
}
